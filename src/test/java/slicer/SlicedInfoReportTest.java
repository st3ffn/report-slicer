package slicer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;
import static slicer.ReportConstants.MAX_SIZE;
import static slicer.ReportConstants.sizeOf;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SlicedInfoReportTest {

    private List<IReportData> reports;

    private IInfoReport source;

    @Mock
    private IReportData dataA, dataB, dataC;

    @Mock
    private QualifiedLogicalName regA, regB, regC;


    @BeforeEach
    void setup() {
        List<IReportValue> values = IntStream.range(0, 20).mapToObj(ReportValue::new)
            .collect(Collectors.toList());
        dataA = new ReportData(regA, values);

        values = IntStream.range(20, 25).mapToObj(ReportValue::new)
            .collect(Collectors.toList());
        dataB = new ReportData(regB, values);

        values = IntStream.range(25, 80).mapToObj(ReportValue::new)
            .collect(Collectors.toList());
        dataC = new ReportData(regC, values);

        reports = Arrays.asList(dataA, dataB, dataC);
        source = new InfoReport(reports);
    }

    @Test
    void name() {
        for (var data : new SlicedInfoReport(source)) {
            assertThat(sizeOf(data), lessThan(MAX_SIZE));
        }
    }
}
