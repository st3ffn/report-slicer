package slicer;

import io.vavr.Tuple2;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

final class SlicedInfoReport implements Iterable<IInfoReport> {

    private final IInfoReport source;

    SlicedInfoReport(IInfoReport source) {
        this.source = source;
    }

    @Override
    public Iterator<IInfoReport> iterator() {
        return new ReportDataSlicer(source.data());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    static final class ReportDataSlicer implements Iterator<IInfoReport> {

        private final Iterator<Tuple2<QualifiedLogicalName, IReportValue>> tupleItr;

        private Optional<QualifiedLogicalName> lastRegister = Optional.empty();
        private Optional<IInfoReport> current;

        ReportDataSlicer(Iterable<IReportData> reportData) {
            List<Tuple2<QualifiedLogicalName, IReportValue>> source = new LinkedList<>();

            reportData.forEach(data ->
                data.values().forEach(value ->
                    source.add(new Tuple2<>(data.register(), value))));
            this.tupleItr = source.iterator();
            current();
        }

        private void current() {
            var builder = new InfoReportBuilder();
            Counter counter = new Counter();
            while (tupleItr.hasNext()) {
                if (!counter.canAddAnotherRegisterAndValue()) {
                    current = builder.build();
                    return;
                }
                var currentTuple = tupleItr.next();

                if (sameAsLastRegister(currentTuple._1)) {
                    counter.addValue();
                } else {
                    counter.addRegisterAndValue();
                }

                builder.add(currentTuple);

                lastRegister = Optional.of(currentTuple._1);
            }

            current = Optional.empty();
        }

        private boolean sameAsLastRegister(QualifiedLogicalName reg) {
            return lastRegister.isPresent() && reg.equals(lastRegister.get());
        }

        @Override
        public boolean hasNext() {
            return current.isPresent();
        }

        @SuppressWarnings("OptionalGetWithoutIsPresent")
        @Override
        public IInfoReport next() {
            // might throw NoSuchElementException if null
            IInfoReport c = current.get();
            current();
            return c;
        }
    }
}
