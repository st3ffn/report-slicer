package slicer;

final class ReportData implements IReportData {

    private final QualifiedLogicalName register;
    private final Iterable<IReportValue> values;

    ReportData(QualifiedLogicalName register, Iterable<IReportValue> values) {
        this.register = register;
        this.values = values;
    }

    @Override
    public QualifiedLogicalName register() {
        return register;
    }

    @Override
    public Iterable<IReportValue> values() {
        return values;
    }

    @Override
    public String toString() {
        return "ReportData{" +
            "register=" + register +
            ", values=" + values +
            '}';
    }
}
