package slicer;

final class InfoReport implements IInfoReport {

    private final Iterable<IReportData> data;

    InfoReport(Iterable<IReportData> data) {
        this.data = data;
    }

    @Override
    public Iterable<IReportData> data() {
        return data;
    }

    @Override
    public String toString() {
        return "InfoReport{" +
            "data=" + data +
            '}';
    }
}
