package slicer;

public interface IInfoReport {

    Iterable<IReportData> data();
}
