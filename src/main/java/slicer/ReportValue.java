package slicer;

final class ReportValue implements IReportValue {

    private final int value;

    ReportValue(int value) {
        this.value = value;
    }

    @Override
    public int value() {
        return value;
    }

    @Override
    public String toString() {
        return "ReportValue{" +
            "value=" + value +
            '}';
    }
}
