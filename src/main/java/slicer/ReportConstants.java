package slicer;

final class ReportConstants {

    static final int MAX_SIZE = 200;
    static final int EMPTY_REPORT_SIZE = 20;
    static final int SIZE_PER_REGISTER = 20;
    static final int SIZE_PER_VALUE = 40;
    private ReportConstants() {
    }

    static int sizeOf(IInfoReport report) {
        int size = EMPTY_REPORT_SIZE;
        for (var data : report.data()) {
            size += SIZE_PER_REGISTER;
            for (var value : data.values()) {
                size += SIZE_PER_VALUE;
            }
        }
        return size;
    }
}
