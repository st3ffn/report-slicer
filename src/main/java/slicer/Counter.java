package slicer;

import static slicer.ReportConstants.MAX_SIZE;
import static slicer.ReportConstants.SIZE_PER_REGISTER;
import static slicer.ReportConstants.SIZE_PER_VALUE;

final class Counter {

    private int current = ReportConstants.EMPTY_REPORT_SIZE;
    private int amountOfRegisters;
    private int amountOfValues;

    void addRegister() {
        amountOfRegisters++;
        current += SIZE_PER_REGISTER;
    }

    void addValue() {
        amountOfValues++;
        current += SIZE_PER_VALUE;
    }

    void addRegisterAndValue() {
        addRegister();
        addValue();
    }

    boolean isBelowMaxSize() {
        return isGivenBelowMaxSize(current);
    }

    private boolean isGivenBelowMaxSize(int given) {
        return given < MAX_SIZE;
    }

    boolean canAddAnotherRegister() {
        return isGivenBelowMaxSize(current + SIZE_PER_REGISTER);
    }

    boolean canAddAnotherValue() {
        return isGivenBelowMaxSize(current + SIZE_PER_VALUE);
    }

    boolean canAddAnotherRegisterAndValue() {
        return isGivenBelowMaxSize(current + SIZE_PER_REGISTER + SIZE_PER_VALUE);
    }

    int amountOfRegisters() {
        return amountOfRegisters;
    }

    int amountOfValues() {
        return amountOfValues;
    }
}
