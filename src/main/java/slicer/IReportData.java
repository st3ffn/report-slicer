package slicer;

public interface IReportData {

    QualifiedLogicalName register();

    Iterable<IReportValue> values();
}
