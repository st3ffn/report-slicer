package slicer;

import io.vavr.Tuple2;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class InfoReportBuilder {

    private final Map<QualifiedLogicalName, List<IReportValue>> map = new HashMap<>();

    InfoReportBuilder add(Tuple2<QualifiedLogicalName, IReportValue> registerValue) {
        var list = map.computeIfAbsent(registerValue._1, k -> new LinkedList<>());
        list.add(registerValue._2);
        return this;
    }

    Optional<IInfoReport> build() {
        List<IReportData> data = new LinkedList<>();
        map.forEach((register, values) -> data.add(new ReportData(register, values)));
        return data.isEmpty() ? Optional.empty() : Optional.of(new InfoReport(data));
    }
}
